import nodemailer from "nodemailer";

export default async function SendMessage(req, res) {
  const { email, message } = req.body;

  let transporter = nodemailer.createTransport({
    port: 465,
    host: "smtp.gmail.com",
    secure: true,
    auth: {
      user: process.env.USER, 
      pass: process.env.PASS, 
    },
  });

  const mailOptions = {
    from: email,
    to: "ali.baloch@launchbox.pk", 
    subject: "Mail FRom Launch Box",
    text: message, 
  };

  transporter.sendMail(mailOptions, function (err, info) {
    if (err) {
      res.status(422).json({ success: false, Error: err });
    } else {
      res.status(200).json({ success: true, info: info });
    }
  });
}
