import React, { useState } from "react";
import { motion } from "framer-motion";
import Image from "next/image";

const Index = () => {
  const [showFullText, setShowFullText] = useState(false);

  const toggleText = () => {
    setShowFullText(!showFullText);
  };

  const containerVariants = {
    hidden: { opacity: 0 },
    visible: { opacity: 1, transition: { duration: 0.5 } },
  };

  const textVariants = {
    hidden: { height: 0 },
    visible: { height: "auto", transition: { duration: 0.5 } },
  };

  return (
    <>
      <div
        className="w-full flex h-2/3 pb-16 mb-16 max-[420px]:block max-[420px]:pr-12 max-[420px]:pl-12 max-[420px]:mt-12"
        id="about"
      >
        <div className="flex place-content-end w-1/2 pr-36 max-[420px]:pr-0 max-[420px]:w-full max-[420px]:block">
          <span className="mt-24">
            <Image src="/assets/aboutbg.svg" height={500} width={500} alt="" />
          </span>
        </div>
        <div className="w-1/2 pr-80 mt-28 max-[420px]:w-full max-[420px]:pr-0 max-[450px]:mt-28">
          <div className="-mt-20 mb-3">
            <p className="text-4xl lg:text-5xl pt-4 font-semibold sm:leading-tight mt-5 max-[450px]:text-3xl max-[450px]:text-center">
              About Us
            </p>
            <p className="text-lg pt-1 font-semibold sm:leading-tight text-center text-beach lg:text-start">
              Unleashing the Potential of Exceptional Online Services
            </p>
          </div>
          <div className="text-container">
            <div
              className={`text-lg pt-4 font-normal lh-33 text-center lg:text-start text-bluegray ${
                showFullText ? "hidden" : ""
              }`}
            >
              {/* Display half of the text initially */}
              At Launchbox, our journey began with a powerful mission in mind:
              to revolutionize the digital landscape and empower businesses to
              thrive in the digital age. We hold a strong belief that technology
              has the potential to revolutionize businesses...
            </div>
            <div
              className={`full-text text-lg pt-4 font-normal lh-33 text-center lg:text-start text-bluegray ${
                showFullText ? "" : "hidden"
              }`}
            >
              {/* Display the full text when "Show More" is clicked */}
              At Launchbox, our journey began with a powerful mission in mind:
              to revolutionize the digital landscape and empower businesses to
              thrive in the digital age. We hold a strong belief that technology
              has the potential to revolutionize businesses of all sizes and
              industries, empowering them to thrive and succeed. Our founders
              have come together with a shared vision to bridge the gap between
              cutting-edge technology and businesses seeking to make their mark
              in the digital realm.
            </div>
            <div className="button-container font-semibold text-[#229680] cursor-pointer max-[450px]:flex max-[450px]:place-content-center">
              {showFullText ? (
                <button className="show-less-btn" onClick={toggleText}>
                  Show Less
                </button>
              ) : (
                <button
                  className="show-more-btn font-semibold text-[#229680] cursor-pointer"
                  onClick={toggleText}
                >
                  Show More
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Index;
