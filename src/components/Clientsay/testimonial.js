import React from "react";

const Testimonial = ({name, des, designation}) => {
  return (
    <section className="mb-2 text-center">
      <div id="carouselExampleCaptions" className="relative">
        <section className="relative isolate overflow-hidden bg-white px-6 py-24 sm:py-32 lg:px-8">
          <div className="absolute inset-0 -z-10 opacity-20" />
          <div className="mx-auto max-w-2xl lg:max-w-4xl">
            <figure className="mt-4">
              <blockquote className="text-center leading-8 text-gray-900 sm:text-2xl sm:leading-9">
                <p className="text-lg max-[450px]:text-sm">
                  “{des}”
                </p>
              </blockquote>
              <figcaption className="mt-4">
                <img
                  className="mx-auto h-10 w-10 rounded-full"
                  src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                  alt=""
                />
                <div className="mt-4 flex items-center justify-center space-x-3 text-base max-[450px]:block">
                  <div className="font-semibold text-gray-900">
                    {name}
                  </div>
                  <svg
                    viewBox="0 0 2 2"
                    width={3}
                    height={3}
                    aria-hidden="true"
                    className="fill-gray-900"
                  >
                    <circle cx={1} cy={1} r={1} className="max-[450px]:hidden"/>
                  </svg>
                  <div className="text-gray-600">{designation}</div>
                </div>
              </figcaption>
            </figure>
          </div>
        </section>
      </div>
    </section>
  );
};

export default Testimonial;
