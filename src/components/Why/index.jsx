import Image from "next/image";

const whydata = [
  {
    heading: "Unmatched Expertise",
    subheading:
      "Skilled professionals delivering customized online services for success",
  },
  {
    heading: "Client-Centric Approach",
    subheading: "Prioritizing your success through trust and collaboration",
  },
  {
    heading: "Customized Solutions",
    subheading:
      "Personalized solutions for thriving businesses in digital realm",
  },
  {
    heading: "Quality Assurance",
    subheading:
      "Excellence delivered through rigorous quality assurance processes",
  },
  {
    heading: "Timely Delivery",
    subheading:
      "Timely delivery of efficient and reliable online solutions",
  },
  {
    heading: "Ongoing Support",
    subheading:
      "Comprehensive support for flawless online presence and growth",
  },
];

const Why = () => {
  return (
    <div id="">
      <div className="mx-auto max-w-7xl px-4 my-20 sm:py-20 lg:px-8">
        <div className="grid grid-cols-1 lg:grid-cols-2">
          {/* COLUMN-1 */}
          <div className="lg:-ml-64 mt-32">
            <Image
              src="/assets/why/iPad1.png"
              alt="iPad-image"
              width={4000}
              height={900}
            />
          </div>

          {/* COLUMN-2 */}
          <div>
            <h3 className="text-4xl lg:text-5xl pt-4 font-semibold sm:leading-tight mt-5 text-center lg:text-start">
              So, Why Choose Us?
            </h3>
            <h4 className="text-lg pt-4 font-normal sm:leading-tight text-center text-beach lg:text-start">
              Unleashing the Potential of Exceptional Online Services
            </h4>

            <div className="mt-10">
              {whydata.map((items, i) => (
                <div className="flex mt-4" key={i}>
                  <div className="rounded-full h-10 w-12 flex items-center justify-center bg-[#9BE8DA] max-[450px]:w-20">
                    <Image
                      src="/assets/why/check.svg"
                      alt="check-image"
                      width={24}
                      height={24}
                    />
                  </div>
                  <div className="ml-5">
                    <h4 className="text-2xl font-semibold">{items.heading}</h4>
                    <h5 className="text-lg text-beach font-normal mt-2">
                      {items.subheading}
                    </h5>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Why;
