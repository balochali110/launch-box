import { Disclosure } from "@headlessui/react";
import { Bars3Icon } from "@heroicons/react/24/outline";
import Link from "next/link";
import Drawer from "./Drawer";
import Drawerdata from "./Drawerdata";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React, { useState, useEffect } from "react";

const navigation = [
  { name: "Home", href: "#home", current: true },
  { name: "About", href: "#about", current: false },
  { name: "Services", href: "#services", current: false },
  { name: "Contact", href: "#contact", current: false },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const Navbar = () => {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const Home = () => {
    router.push("/");
  };
  const [isScrolled, setIsScrolled] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.pageYOffset;
      if (scrollTop > 0) {
        setIsScrolled(true);
      } else {
        setIsScrolled(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <>
      <div
        className={
          isScrolled
            ? "navbar mx-auto lg:py-4 lg:px-8 !bg-white w-full"
            : "navbar mx-auto px-6 lg:py-4 lg:px-8 w-full"
        }
      >
        <div className="flex h-20 items-center justify-between ml-72 max-[450px]:ml-8">
          <div className="flex flex-1 items-center sm:items-stretch sm:justify-start">
            <div
              className="flex flex-shrink-0 items-center cursor-pointer"
              onClick={Home}
            >
              <Image
                src="/assets/logo.png"
                width={250}
                height={100}
                alt="launchbox log"
              />
            </div>

            <div className="hidden lg:block m-auto">
              <div className="flex space-x-4">
                {navigation.map((item) => (
                  <Link
                    key={item.name}
                    href={item.href}
                    className={classNames(
                      item.current
                        ? " text-black hover:opacity-100"
                        : "hover:text-black hover:opacity-100",
                      "px-3 py-4 text-lg font-normal opacity-75 space-links"
                    )}
                    aria-current={item.href ? "page" : undefined}
                  >
                    <p
                      className={
                        isScrolled ? "text-black" : "text-white font-semibold"
                      }
                    >
                      {item.name}
                    </p>
                  </Link>
                ))}
              </div>
            </div>
          </div>
          <div className="mr-2 min-[450px]:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.8}
              stroke={isScrolled ? "black" : "white"}
              className="w-6 h-6"
              onClick={() => setIsOpen(true)}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
              />
            </svg>
          </div>

          <Drawer isOpen={isOpen} setIsOpen={setIsOpen}>
            <Drawerdata />
          </Drawer>
        </div>
      </div>
    </>
  );
};

export default Navbar;
